var express = require('express');
var bodyParser = require('body-parser');
var requestJson = require('request-json');
var config = require('./config.js');
var loginController = require('./controllers/login.js');
var userController = require('./controllers/users.js');
var accountController = require('./controllers/account.js');
var transactionController = require('./controllers/transaction.js');
var validator = require('express-validator');
var middlewareController = require('./controllers/middleware.js');
var cors = require('cors');

var app = express();
app.use(validator());
app.use(bodyParser.json());
app.use(cors());

var port = process.env.PORT || 3000;

var urlMlabRaiz = config.urlMlabRaiz;
var apiKey = config.apiKey;
var query = config.query;
const URI = config.URI;

app.listen(port);
console.log('Escuchando en el puerto 3000');

//app.use(middlewareController.middleware);

var clienteMlab;

//USERS
app.get(URI + 'usersmlab', userController.get);
//app.get(URI + 'usersmlab', middlewareController.middleware, userController.get);
app.get(URI + 'usersmlab/:id', userController.getId);
app.post(URI + 'usersmlab', userController.post);
app.put(URI + 'usersmlab/:id', userController.putId);
app.delete(URI + 'usersmlab/:id', userController.deleteId);
app.get(URI + 'usersmlab/:id/account', userController.getUserAccounts);

//ACCOUNTS
app.get(URI + 'usersmlab', accountController.getAccounts);
app.get(URI + 'usersmlab', accountController.getAccount);

//TRANSACTIONS
app.get(URI + 'usersmlab/:id/accounts/:id_account/transactions', transactionController.getTransactions);
app.post(URI + 'usersmlab/:id/accounts/:id_account/transaction', transactionController.doTransaction);

//LOGIN
app.post(URI + 'login', loginController.login);
//LOGOUT
app.put(URI + 'logout',loginController.logout);
