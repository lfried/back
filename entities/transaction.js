var Resultado = require('./resultado.js');

/** Trasaction representa un movimiento que afecta el saldo de una cuenta */
class Transaction {

  setProperties(obj) {
    this.idMovimiento = obj.numero;
    this.fecha = obj.date;
    this.descripcion = obj.description;
    this.importe = obj.amount;
  }

  setImporte(importe){
    this.importe=importe;
  }

  getImporte(){
    return this.importe;
  }

  setDescripcion(descripcion){
    this.descripcion = descripcion;
  }

  getDescripcion(){
    return this.descripcion;
  }

  setTimestamp(timeStamp){
    this.timestamp = timeStamp;
  }

  getTimestamp(){
    return this.timestamp;
  }

  setIdMovimiento(numero){
  this.idMovimiento = numero;
  }

  getIdMovimiento(){
    return this.idMovimiento;
  }


  validarDatos() {
    let resultado = new Resultado();

    if (this.importe == undefined) {
      resultado.setOk(false);
      resultado.addMensaje("Debe indicar el monto  de la transacción");
    } else if (isNaN(this.monto)) {
      resultado.setOk(false) ;
      resultado.addMensaje("El monto de la transacción debe ser numérico");
    }
    //Valido el Concepto
    if (this.concepto == undefined) {
      resultado.setOk(false) ;
      resultado.addMensaje("Debe indicar el concepto de la transacción");
    }

    return resultado;
  }

}

module.exports = Transaction;
