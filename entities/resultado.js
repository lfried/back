var Mensaje = require('./mensaje.js');

class Resultado{
  constructor(){
    this.ok = true;
  }

  getOK(){
    return this.ok;
  }

  setOK(ok){
    this.ok = ok;
  }

  getMensajes(){
    return  this.mensajes;
  }

  setData(data){
    this.data = data;
  }

  addMensaje(mensaje){
    this.ok = false;
    let msg = new Mensaje(mensaje);
    if (this.mensaje == undefined){
      this.mensaje = [];
    }
    this.mensaje.push(msg);
  }
}

module.exports = Resultado;
