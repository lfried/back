var config = require('../config.js');
var Resultado = require('./resultado.js');
//var validateCurrencyCode = require('validate-currency-code');

class Account {

  setProperties(obj){
    this.idCuenta = obj.idCuenta;
    this.idUsuario = obj.idUsuario;
    this.transactions = obj.transactions;
    if(obj.balance == undefined){ // Se asinga 0 como valor por defecto al saldo
      this.balance = 0;
    }else{
      this.balance = Number(obj.balance);
    }
    this.ultNroMovimiento = obj.ultNroMovimiento;
  }

  setIdCuenta(idCuenta){
    this.idCuenta = idCuenta;
  }

  getIdCuenta(){
    return this.idCuenta;
  }

  setIdUsuario(idUsuario){
    this.idUsuario = idUsuario;
  }

  getIdUsuario(){
    return this.idUsuario;
  }


  setBalance(balance){
    this.balance = balance;
  }

  getBalance(){
    if(this.balance==undefined){
      this.balance = 0;
    }
    return this.balance;
  }

  setUltNroMovimiento(ultNroMovimiento){
    this.ultNroMovimiento = ultNroMovimiento;
  }

  getUltNroMovimiento(){
    return this.ultNroMovimiento;
  }

  /** Incrementa último número de movimiento */
  incUltNroMovimiento(){
    if (this.ultNroMovimiento == undefined){
      this.ultNroMovimiento = 1
    }else {
      this.ultNroMovimiento++;
    }
  }

  /** Getter */
  getTransactions(){
    if(this.transactions == undefined){
      this.transactions = [];
    }
    return this.transactions;
  }

  setTransactions(transactions){
    this.transactions = transactions;
  }

  /** Agrega un movimiento */
  addTransaction(transaction){
    if (this.transactions == undefined){
      this.transactions = [];
    }

    var camposSet = {
      "idMovimiento": transaction.idMovimiento,
      "fecha": transaction.fecha,
      "descripcion": transaction.descripcion,
      "importe": transaction.importe
    };

    this.transactions.push(camposSet);

  }

  // initAccount(cliente, moneda, nombre, numero){
  //   let idCta = 'CTA-' + cliente + '-' + moneda + '-' + numero;
  //   this.id = idCta;
  //   this.moneda = moneda;
  //   this.nombre = nombre;
  //   this.saldo = 0;
  // }

  /** Valida los datos necesarios para crear una cuenta */
  validarDatos(){
    let resultado = new Resultado();

    if(this.saldo != undefined && isNaN(this.saldo)){
      resultado.addError("El saldo debe ser numérico");
    }
    return resultado;
  }
}

module.exports = Account;
