var bcrypt = require('bcryptjs');
//var bcrypt = require('bcrypt-nodejs');

var config = require('../config.js');
var Resultado = require('./resultado.js');

class User {
  setProperties(obj){
    this.id = obj.id;
    this.firstName =  obj.firstName;
    this.lastName = obj.lastName;
    this.email = obj.email;
    this.password = obj.password;
  }

  getId(){
    return this.id;
  }

  setId(id){
    this.id = id;
  }

  getFirstName(){
    return this.firstName;
  }

  setFirstName(firstName){
    this.firstName = firstName;
  }

  getLastName(){
    return this.lastName;
  }

  setLastName(lastName){
    this.lastName = lastName;
  }

  getEmail(){
    return this.email;
  }

  setEmail(email){
    this.email = email;
  }

  getPassword(){
    return this.password;
  }

  setPassword(password){
    this.password  = bcrypt.hashSync(password, config.BCRYPT_SALT_ROUNDS);
  }

  validarDatos(){
    var resultado = new Resultado();

    if(this.firstName == undefined){
      resultado.addError("El campo firstName es obligatorio");
    }
    if(this.lastName == undefined){
      resultado.addError("El campo lastName es obligatorio");
    }
    if (this.password == undefined){
      resultado.addError("El password es obligatorio");
    }else if(this.password.length < 8){
      resultado.addError("El largo mínimo del password es 8");
    }
    if (this.email == undefined){
      resultado.addError("El campo email es obligatorio");
    }else if(!mailValidator.validate(this.email)){
      resultado.addError("El formato de email incorrecto");
    }
    return resultado;
  }

}

module.exports = User;
