# imagen docker base
FROM node

# Definidmos directorio de trabajo de docker
WORKDIR /docker.api

# Agregar archivos al proyecto
ADD . /docker.api

# Para instalar las dependencias del proyecto
# RUN npm install

# puerto donde exponemos contenedor
EXPOSE 3000

# comando para lanzar la app
CMD ["npm" , "start"]
