var config = require('../config.js');
var urlMlabRaiz = config.urlMlabRaiz;
var apiKey = config.apiKey;
const URI = config.URI;
var bodyParser = require('body-parser'); //Móudlo para parseo del body del los request
var requestJson = require('request-json'); //Cliente http para comunicación con el api MLab
//var bcrypt = require('bcrypt'); //Módulo de encriptación de passwords
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
var User = require('../entities/user.js');
var util = require('./utils.js');
var Resultado = require('../entities/resultado.js');


function login(req, res){
    var email = req.body.email;
    var pass = req.body.password;
    let resultado = new Resultado();
    var queryStringEmail = 'q={"email":"' + email + '"}&';

    var  clienteMlab = requestJson.createClient(urlMlabRaiz + '/user?'+ queryStringEmail + "&l=1&" + apiKey);

    clienteMlab.get('' ,function(error, respuestaM , body) {
      var respuesta = body[0];

      if(respuesta != undefined){

          if (bcrypt.compareSync(pass, respuesta.password)){
            var session = {"logged":true};
            var login = '{"$set":' + JSON.stringify(session) + '}';
            clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + apiKey, JSON.parse(login),
                function(errorP, respuestaM, bodyP) {

                  var user = {
                    "id": respuesta.id,
                    "email": email
                  };
                  let bodyEnviar = body[0];
                  let jwt = util.createJWT(user);
                  res.status(200).send({bodyEnviar, jwt});
                  //res.send(body[0]);
                });
          }
          else {
            //res.send({"msg":"contraseña incorrecta"});
            resultado.setOK(false);
            resultado.addMensaje("contraseña incorrecta");
            res.status(404);
            res.send(resultado);
          }
      } else {
        //res.send({"msg": "email Incorrecto"});
        resultado.setOK(false);
        resultado.addMensaje("email Incorrecto");
        res.status(404);
        res.send(resultado);
      }
    });
};


function logout(req, res){
   console.log("Ejecutando logout");
   let resultado = new Resultado();

   var email = req.body.email;
   console.log(email);
   var queryStringEmail = 'q={"email":"' + email + '"}&';

   clienteMlab = requestJson.createClient(urlMlabRaiz + '/user?'+ queryStringEmail + "&l=1&" + apiKey);
   clienteMlab.get('' ,function(error, respuestaMLab, body) {
     console.log("entro al get");

     var respuesta = body[0];
     console.log(respuesta);

     if(respuesta != undefined){
           console.log("logout Correcto");

           var session = {"logged":false};
           var logout = '{"$unset":' + JSON.stringify(session) + '}';
           console.log(logout);

           clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + apiKey, JSON.parse(logout),
             function(errorP, respuestaMLabP, bodyP) {
               if(!errorP){
                 res.send(body[0]);
               }else{
                 //res.status(500).send('Ocurrió un error en el login');
                 resultado.setOK(false);
                 resultado.addMensaje("Ocurrió un error al realizar el logout");
                 res.status(500);
                 res.send(resultado);
               };
             });
     } else {
       //res.status(500).send('Ocurrió un error al realizar el logout');
       resultado.setOK(false);
       resultado.addMensaje("Ocurrió un error al realizar el logout");
       res.status(500);
       res.send(resultado);
     }
   });
 };

module.exports.login = login;
module.exports.logout = logout;
