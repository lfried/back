var config = require('../config.js');
var bodyParser = require('body-parser'); //Móudlo para parseo del body del los request
var requestJson = require('request-json'); //Cliente http para comunicación con el api MLab
var bcrypt = require('bcryptjs'); //Módulo de encriptación de passwords
//var bcrypt = require('bcrypt-nodejs');
//var mailValidator = require('email-validator'); //Módulo de validación de formato de emails
var User = require('../entities/user.js');
var Resultado = require('../entities/resultado.js');

var urlMlabRaiz = config.urlMlabRaiz;
var apiKey = config.apiKey;
var query = config.query;
var generate_id=0;

function get(req, res) {
  let resultado = new Resultado();

  let clienteMlab = requestJson.createClient(urlMlabRaiz + "/user?f={'_id':0}&" + apiKey);

  clienteMlab.get('', function(err, resM, body) {
    if (!err){
      generate_id=body.length;
      res.status(200);
      res.send(body);
    }
    else {
      resultado.setOK(false);
      resultado.addMensaje("Error al obtener los usuarios");
      res.status(404);
      res.send(resultado);
    };
  });
}

// GET USER MLAB
function getId (req,res) {
   let resultado = new Resultado();
   var errors = req.validationErrors();

   if (errors) {
     resultado.setOK(false);
     resultado.addMensaje("Parámetros incorrectos");
     res.status(404);
     res.send(resultado);
     return;
   }

   var id = req.params.id;
   var query = 'q={"id":' + id + '}';
   let clienteMlab = requestJson.createClient(urlMlabRaiz + "/user?f={'_id':0}&" + query + "&l=1&" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
        if (!err) {
            if (body.length > 0){
              //res.send(body[0]);
              resultado.setOK(true);
              resultado.setData(body[0]);
              res.status(200);
              res.send(body[0]);
            }else {
              resultado.setOK(false);
              resultado.addMensaje("Usuario no encontrado");
              res.status(404);
              res.send(resultado);
            }
        } else {
          resultado.setOK(false);
          resultado.addMensaje("Usuario no encontrado");
          res.status(404);
          res.send(resultado);
        }
    });
};


function post(req, res){
  console.log('post');
  let resultado = new Resultado();
  let newID;

  req.checkBody("first_name", "Nombre no debe ser vacio").notEmpty();
  req.checkBody("last_name", "Apellido no debe ser vacio").notEmpty();
  req.checkBody("email", "Direcciòn de correo invalida").isEmail();
  req.checkBody('password', 'EL password debe tener un minimo de 8 y tener al menos un caracter').isLength({ min: 8 }).matches(/\d/);

  var errors = req.validationErrors();
  console.log(errors);
  //if (errors) {res.status(400).send(errors);return;}

  if (errors) {
    resultado.setOK(false);
    resultado.addMensaje("Parámetros incorrectos");
    res.status(404);
    res.send(resultado);
    return;
  }

  let clienteMlab = requestJson.createClient(urlMlabRaiz + "/user?f={'_id':0}&" + query + apiKey);

  clienteMlab.get('', function(error, resM, body){
    if (!error) {
      if (body.length > 0) {
        console.log(body.length);
        newID = body.length + 1;
        console.log("newID:" + newID);
        var newUser = {
          "id" : newID,
          "first_name" : req.body.first_name,
          "last_name" : req.body.last_name,
          "email" : req.body.email,
          //"password" : req.body.password
          "password" : bcrypt.hashSync(req.body.password, config.BCRYPT_SALT_ROUNDS)
        };
        console.log(newUser);
        clienteMlab.post(urlMlabRaiz + "/user?" + apiKey, newUser,
          function(error, resM, body){
            if (!error) {
              console.log(error);
                  //res.send(body);
                  resultado.setOK(true);
                  resultado.setData(newUser);
                  res.status(200);
                  res.send(resultado);
            }else {
              resultado.setOK(false);
              resultado.addMensaje("Ha ocurrido un error al agregar el usuario");
              res.status(404);
              res.send(resultado);
            }
          });
        }else {
          resultado.setOK(false);
          resultado.addMensaje("Ha ocurrido un error al obtener los usuarios");
          res.status(404);
          res.send(resultado);
        }
      }else {
        resultado.setOK(false);
        resultado.addMensaje("Ha ocurrido un error al obtener los usuarios");
        res.status(404);
        res.send(resultado);
      }
    });
};

// put USER  putMLAB
function putId(req, res){

  let resultado = new Resultado();
  let id = req.params.id;
  var queryFiltro = 'q={"id":' + id + '}';

  let firstName = req.body.first_name;
  let lastName = req.body.last_name;
  let email = req.body.email;
  let password = req.body.password;
  var usuarioUpdate = {};

  req.checkBody("id", "Debe ingresar el id que desea modificar").notEmpty();
  req.checkBody("first_name", "Nombre no debe ser vacio").notEmpty();
  req.checkBody("last_name", "Apellido no debe ser vacio").notEmpty();
  req.checkBody("email", "Direcciòn de correo invalida").isEmail();
  req.checkBody('password', 'EL password debe tener un minimo de 8 y tener al menos un caracter').isLength({ min: 8 }).matches(/\d/);

  var errors = req.validationErrors();

  //if (errors) {res.status(400).send(errors);return;}
  console.log(errors);

  if (errors) {
    resultado.setOK(false);
    resultado.addMensaje("Parámetros incorrectos");
    console.log(resultado);
    res.status(404);
    res.send(resultado);
    return;
  }

  if (firstName != undefined && firstName.length > 0) {
   usuarioUpdate.first_name = firstName;
  }
  if (lastName != undefined && lastName.length > 0) {
   usuarioUpdate.last_name = lastName;
  }
  if (email != undefined && email.length > 0) {
   usuarioUpdate.email = email;
     console.log(usuarioUpdate.email);
  }
  if (password != undefined && password.length > 7) {
   // Se encripta el password antes de presistirlo
   usuarioUpdate.password = bcrypt.hashSync(password, config.BCRYPT_SALT_ROUNDS)
  }

  let clienteMlab = requestJson.createClient(urlMlabRaiz + "/user?f={'_id':0}&" + queryFiltro + "&l=1&" + apiKey);
  clienteMlab.get('', function(error, resM, body){

    if (!error) {
      if (body.length > 0){
        //var usuarioUpdate=req.body;
        usuarioUpdate.id= body[0].id;//el body que recibo (la respuesta) viene como array

        clienteMlab = requestJson.createClient(urlMlabRaiz + "/user");
        clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey,usuarioUpdate ,
           function(errP, resP, bodyP) {
             if (!errP) {
               if (body != undefined > 0) {
                 resultado.setOK(true);
                 resultado.setData(bodyP);
                 res.status(200);
                 res.send(resultado);

               } else {
                resultado.setOK(false);
                resultado.addMensaje("Usuario no encontrado");
                res.status(404);
                res.send(resultado);
              }
             }else{
               resultado.setOK(false);
               resultado.addMensaje("Error al actualizar el usuario");
               res.status(500);
               res.send(resultado);
             }
          })
      }else {
        resultado.setOK(false);
        resultado.addMensaje('Usuario no encontrado');
        res.status(404);
        res.send(resultado);
      }
    }else{
      resultado.setOK(false);
      resultado.addMensaje("Error obteniendo usuario");
      res.status(500);
      res.send(resultado);
    }
  });
};

//delete USERS
function deleteId(req, res){

 let id = req.params.id;
 let resultado = new Resultado();

 req.checkBody("id", "debe ingresar un id valido").notEmpty();

 var errors = req.validationErrors();

 if (errors) {
   resultado.setOK(false);
   resultado.addMensaje("Parámetros incorrectos");
   res.status(404);
   res.send(resultado);
   return;
 }

 var queryFiltro = 'q={"id":' + id + '}';
 let clienteMlab = requestJson.createClient(urlMlabRaiz + '/user?'+ queryFiltro + "&l=1&" + apiKey);

 clienteMlab.get('', function(error, resM, body){

   if (!error) {
     if (body.length > 0){
       var idUsuarioMongo=body[0]._id.$oid;
       clienteMlab = requestJson.createClient(urlMlabRaiz + "/user/");

       clienteMlab.delete(idUsuarioMongo + '?' + apiKey ,
          function(errP, resP, bodyP) {
            if (!errP) {
              resultado.setOK(true);
              resultado.setData(bodyP);
              res.status(200);
              res.send(resultado);
            }else{
              resultado.setOK(false);
              resultado.addMensaje("Error al eliminar usuario");
              res.status(500);
              res.send(resultado);
            }
         })
     }else {
       resultado.setOK(false);
       resultado.addMensaje("Usuario no encontrado");
       res.status(404);
       res.send(resultado);
     }
   }else{
     resultado.setOK(false);
     resultado.addMensaje("Error obteniendo usuario");
     res.status(500);
     res.send(resultado);
   }
 });
};

//GET ALL ACCOUNT USER BY ID
function getUserAccounts(req,res){
  let resultado = new Resultado();

  var id = req.params.id;
  var query = 'q={"idUsuario":' + id + '}';
  let clienteMlab = requestJson.createClient(urlMlabRaiz + "/account?f={'_id':0}&" + query + "&l=1&" + apiKey);

    clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        resultado.setOK(true);
        resultado.setData(body);
        res.status(200);
        res.send(body);
      }
      else {
        resultado.setOK(false);
        resultado.addMensaje("Error al obtener las cuentas del usuario");
        res.status(500);
        res.send(resultado);
      };
    });
  };



module.exports.get = get;
module.exports.getId = getId;
module.exports.putId = putId;
module.exports.post = post;
module.exports.deleteId = deleteId;
module.exports.getUserAccounts = getUserAccounts;
