const jwt = require('jsonwebtoken');
var config = require('../config.js');
//const jwt=require('jwt-simple');
//const moment=require('moment');

exports.createJWT = function createJWT(user) {
  const token = jwt.sign(
    {
      id: user.id,
      email: user.email
    },
    config.JWT_SECRET,
    {
      expiresIn: config.JWT_EXPIRE
    }
  );
return token;
}
