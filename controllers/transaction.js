var requestJson = require('request-json');
const config=require('../config');
var Transaction = require('../entities/transaction.js');
var Account = require('../entities/account.js');
var Resultado = require('../entities/resultado.js');

var urlMlabRaiz = config.urlMlabRaiz;
var apiKey = config.apiKey;
var query = config.query;

function getTransactions(req,res){
    var id = req.params.id;
    var query = 'q={"idUsuario":' + id + '}';
    let clienteMlab = requestJson.createClient(urlMlabRaiz + "/account?f={'_id':0}&" + query + "&l=1&" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        res.send(body[0]);
      }
      else {
        res.status(404).send('Error al obtener las cuentas del usuario');
      };
    });
}

function doTransaction(req, res){

   let resultado = new Resultado();
   var errors = req.validationErrors();

   console.log(req.body);

   req.checkBody("date", "debe ingresar una fecha valida").notEmpty();
   req.checkBody("amount", "debe ingresar un importe valido").notEmpty();
   req.checkBody("description", "debe ingresar una descripcion valido").notEmpty();

   var errors = req.validationErrors();

   if (errors) {
     resultado.setOK(false);
     resultado.addMensaje("Parámetros incorrectos");
     console.log(resultado);
     res.status(404);
     res.send(resultado);
   }

  let idUsuario = req.params.id; //Usuario
  let idCuenta = req.params.id_account; //Cuenta

  let transaction = new Transaction();
  transaction.setProperties(req.body);
  //transaction.setTimestamp(new Date());

  var queryFiltroCuenta = 'q={"idUsuario":' + idUsuario + '}';

  //Obtengo las cuentas del usuario logueado
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/account?f={'_id':0}&" + queryFiltroCuenta + "&l=1&" + apiKey);
  clienteMlab.get('', function(errorCuenta, resM, bodyCuenta){

    if (!errorCuenta) {

      if (bodyCuenta.length > 0){

        var saldoActual = bodyCuenta[0].balance
        let cuenta = new Account();
        cuenta.setProperties(bodyCuenta[0]);

        let movimientos = [];
        let idMovimiento = 0;

        for(let i=0; i<bodyCuenta[0].transactions.length;i++){
          idMovimiento = bodyCuenta[0].transactions[i].idMovimiento;
        }

        transaction.setIdMovimiento(++idMovimiento);
        cuenta.addTransaction(transaction);
        cuenta.setBalance(saldoActual + parseFloat(req.body.amount));
        clienteMlab = requestJson.createClient(urlMlabRaiz + "/account");

        clienteMlab.put('?q={"idCuenta": ' + bodyCuenta[0].idCuenta + '}&' + apiKey, cuenta ,
           function(errP, resP, bodyP) {
               if (!errP) {
                 res.send(bodyP);
               }else{
                 res.status(500).send('Error al acreditar el importe');
               }
           })
      }
      else {
        res.status(404).send('Cuenta no encontrada');
      }
    }
    else{
      res.status(500).send('Error obteniendo la cuenta');
    }
  });
};

module.exports={
  getTransactions,
  doTransaction
};
