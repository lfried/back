var config = require('../config.js');
var Resultado = require('../entities/resultado.js');
var jwt = require('jsonwebtoken');
const services=require('./utils');
const URI = config.URI; // '/api-uruguay/v1/';

// function middleware(req, res, next){
//   if(!req.headers.authorization)
//     return res.status(403).send({message:'No tienes autorizacion'});
//     var token = req.headers.authorization.split(" ")[1];
//     services.decodeToken(token)
//       .then(response=>{
//         req.user=response;
//         next();
//       })
//       .catch(response=>{
//         res.status(response.status).send(response.message);
//       })
//   }


function middleware(req, res, next) {

  resultado = new Resultado();  // Respuesta estándar
  /** Verifico si el PATH es a /LOGIN, si no lo es verifico que venga el JWT y sea válido */
  let pathLogin = URI + 'login';
  let pathUsers = URI + 'users';
  let pathAccount = URI + 'account';

  if( !( req.method == 'POST' && ( req.originalUrl == pathLogin || req.originalUrl == pathUsers || req.originalUrl == pathAccount))){
      let token = req.get('Authorization'); // obtengo el token jwt
      if(token==undefined){
          res.status(401);
          resultado.setOk(false);
          resultado.addMensaje('Acceso no autorizado, por favor autentifíquese');
          res.send(resultado);
      }else{
        try{
          let enLista = false
          for(let i = 0; i < listaJWT.length && enLista; i++){
            if(listaJWT[i] == token){
              enLista = true;
            }
          }
          if(!enLista){
            jwt.verify(token,config.jwtClave,function(err,decode){
              if(err){
                res.status(401);
                resultado.setOk(false);
                resultado.addMensaje('Error en la verificación del token de seguridad');
                res.send(resultado);
              }else{
                next();
              }
            });
          }else{
            res.status(401);
            resultado.setOk(false);
            resultado.addMensaje('Token de seguridad inválido');
            res.send(resultado);
          }
        }catch(errorjwt){
          res.status(401);
          resultado.setOk(false);
          resultado.addMensaje('No se pudo validar token JWT, autentíquese para generar un nuevo token');
          res.send(resultado);
        }
      }
  }else{
    next();
  }
}



module.exports.middleware = middleware;
