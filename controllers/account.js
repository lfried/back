var requestJson = require('request-json');
const config=require('../config');

var urlMlabRaiz = config.urlMlabRaiz;
var apiKey = config.apiKey;
var query = config.query;


//GET ALL ACCOUNTS
function getAccounts(request,response){
    let clienteMlab = requestJson.createClient(urlMlabRaiz + "/accounts?f={'_id':0}&" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        res.send(body);
      }
      else {
        res.status(404).send('Error al obtener las cuentas');
      };
    });
}

function getAccount(request,response){
    var client = requestJson.createClient(url);
    const queryName='q={"IBAN":"'+request.params.id+'"}&';
    client.get(config.mlab_collection_account+'?'+queryName+config.mlab_key, function(err, res, body) {
    var respuesta=body[0];
    response.send(respuesta);
    });
}

module.exports={
  getAccounts,
  getAccount
};
